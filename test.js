var assert = require('assert');
var mongoToSql = require('./mongo-util').mongoToSql;
const INVALID_MONGO_STRING =  require('./mongo-util').INVALID_MONGO_STRING;
const INVALID_MONGO_COLLECTION_NAME =  require('./mongo-util').INVALID_MONGO_COLLECTION_NAME;
describe('Array', function() {

    it('should be able to catch invalid collection name', ()  => {
        assert.throws(() => mongoToSql("db.u$er.find({name: 'julio'}););"), Error, INVALID_MONGO_COLLECTION_NAME);
    });

    // it('should be able to handle no where clause', ()  => {
    //     assert.throws(() => mongoToSql("db.user.find({}););"), Error, INVALID_MONGO_COLLECTION_NAME);
    // });

    it('should be able to catch invalid mongo string', ()  => {
        assert.throws(() => mongoToSql("db.user.find);"), Error, INVALID_MONGO_STRING);
    });

    it('should only support mongo find', ()  => {
        assert.throws(() => mongoToSql("db.user.foo({name: 'julio'});"), Error, INVALID_MONGO_STRING);
    });

    it('should be able to deal with some string padding', ()  => {
        let result = mongoToSql("    db.user.find(  {name: 'julio' } );  ");
        assert.equal(result, "SELECT * FROM user WHERE name = 'julio';");
    });

    it('should be able to convert simple scenario', ()  => {

        let result = mongoToSql("db.user.find({name: 'julio'});")
        assert.equal(result, "SELECT * FROM user WHERE name = 'julio';");
    });

    it('should be able to convert with projection', ()  => {
        let result = mongoToSql("db.user.find({_id: 23113},{name: 1, age: 1});");
        assert.equal(result, "SELECT name, age FROM user WHERE _id = 23113;");
    });

    it('should be able to convert with where clause and projection', ()  => {

        let result = mongoToSql("db.user.find({age: {$gte: 21}},{name: 1, _id: 1});")
        assert.equal(result,  "SELECT name, _id FROM user WHERE age >= 21;");
    });

    it('should be able to support $lt, $lte, $gt, $gte', ()  => {

        let result = mongoToSql("db.user.find({age: {$gte: 21}},{name: 1, _id: 1});")
        assert.equal(result,  "SELECT name, _id FROM user WHERE age >= 21;");

        let result2 = mongoToSql("db.user.find({age: {$gt: 21}},{name: 1, _id: 1});")
        assert.equal(result2,  "SELECT name, _id FROM user WHERE age > 21;");

        let result3 = mongoToSql("db.user.find({age: {$lt: 21}},{name: 1, _id: 1});")
        assert.equal(result3,  "SELECT name, _id FROM user WHERE age < 21;");

        let result4 = mongoToSql("db.user.find({age: {$lte: 21}},{name: 1, _id: 1});")
        assert.equal(result4,  "SELECT name, _id FROM user WHERE age <= 21;");
    });

    it('should be able to support $ne', ()  => {

        let result = mongoToSql("db.user.find({age: {$ne: 21}},{name: 1, _id: 1});")
        assert.equal(result,  "SELECT name, _id FROM user WHERE age <> 21;");
    })

    it('should be able to support $in', ()  => {
        let result = mongoToSql("db.user.find({age: {$in: [21, 22]}},{name: 1, _id: 1});")
        assert.equal(result,  "SELECT name, _id FROM user WHERE age in (21,22);");
        
        let result2 = mongoToSql("db.user.find({name: {$in: ['joe', 'bob']}},{name: 1, _id: 1});")
        assert.equal(result2,  "SELECT name, _id FROM user WHERE name in ('joe','bob');");
    });

    it('should be able to support $and', ()  => {

        let strQuery = 'db.inventory.find({ $and: [ { inStock: 0 }, { price: { $gte: 4.99 } }]});'
        let strSql = 'SELECT * FROM inventory WHERE (inStock = 0 AND price >= 4.99);'
        let result = mongoToSql(strQuery)
        assert.equal(result,  strSql);
    });
       
    it('should be able to support $or', ()  => {

        let strQuery = 'db.inventory.find({ $or : [ { price : 0.99 }, { price : 1.99 } ] });';
        let strSql = 'SELECT * FROM inventory WHERE (price = 0.99 OR price = 1.99);'
        let result = mongoToSql(strQuery)
        assert.equal(result,  strSql);
    });

    it('should be able to support nested both $and $or', ()  => {

        let strQuery = 'db.inventory.find({ $and : [ { $or : [ { price : 0.99 }, { price : 1.99 } ] },{ $or : [ { sale : true }, { qty : { $lt : 20 } } ] }]});';
        let strSql = 'SELECT * FROM inventory WHERE ((price = 0.99 OR price = 1.99) AND (sale = true OR qty < 20));'
        let result = mongoToSql(strQuery)
        assert.equal(result,  strSql);
    });
    
});