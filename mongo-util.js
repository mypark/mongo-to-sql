const Hjson = require('hjson');

const INVALID_MONGO_STRING = 'invalid mongo string';
const INVALID_MONGO_COLLECTION_NAME = 'invalid mongo collection name';
exports.INVALID_MONGO_STRING = INVALID_MONGO_STRING;
exports.INVALID_MONGO_COLLECTION_NAME = INVALID_MONGO_COLLECTION_NAME;

exports.mongoToSql = (mongoString) => {
    //trim white space
    mongoString = mongoString.trim();

    //todo be more relaxed about white space in between, enhancement for later
   
    // check for a valid mongo string
    let { findParameters, collectionName } = parseMongoString(mongoString);
    
    // where clause is the first and required parameter to find
    let whereParameters = findParameters[0];
    let sqlWhereClauses = getWhereSql(whereParameters);
        
    // projection is an optional 2nd parameter to find
    let projection = '*';
    let projectionParameters = findParameters[1];
    if (projectionParameters) {
        projection = '';
        Object.keys(projectionParameters).forEach((key) => {
            if (projectionParameters[key] === 1) {
                projection += key + ', ';
            }
        });
        projection = projection.slice(0, -2);
    }

    let result = `SELECT ${projection} FROM ${collectionName}`;
    if (sqlWhereClauses.length > 0)
        result += ` WHERE ${sqlWhereClauses};`;
    return result;
};

// generate the sql string from conditions
// comparotor is an optional parameter that lets us know
// that we are in a where clause for a specific property
// which helps us keep track of what we are doing in a recursion
const getWhereSql = (conditions, comparotor) => {

    if (typeof conditions === 'object') {
        let sqlClauses = [];

        // todo reduce the $or and $and logic into a single helper function as there is some
        // repetition, maybe I can generalize as well
        if (conditions.$or){
            let sql = getSqlForAndOr(conditions, '$or');
            sqlClauses.push(sql);
        } else if (conditions.$and) {
            let sql = getSqlForAndOr(conditions, '$and');
            sqlClauses.push(sql);
        } else if (comparotor) {
            sqlClauses = [...sqlClauses, ...getSqlForComparator(conditions, comparotor, sqlClauses)];
        } else {
            let sql = Object.keys(conditions).reduce((accumulator, key) => {
                accumulator.push(getWhereSql(conditions[key], key));
                return accumulator;
            }, []);
            sqlClauses.push(sql);
        }       
        // join AND on the implicit AND
        return `${sqlClauses.join(' AND ')}`;
    } else {
        return `${comparotor} = ${sqlEncode(conditions)}`;
    }
};

// sql escape string from CUBRID RDBMS
const escapeSqlString = (val) => {
    if (typeof(val) !== 'string')
        return val;

    val = val.replace(/[\0\n\r\b\t\\'"\x1a]/g, function (s) {
        switch (s) {
          case "\0":
            return "\\0";
          case "\n":
            return "\\n";
          case "\r":
            return "\\r";
          case "\b":
            return "\\b";
          case "\t":
            return "\\t";
          case "\x1a":
            return "\\Z";
          case "'":
            return "''";
          case '"':
            return '""';
          default:
            return "\\" + s;
        }
      });
    
      return val;
};

const sqlEncode = (sql) => {
    if (typeof(sql) === 'string') { 
        sql = escapeSqlString(sql);
        return "'" + sql + "'";
    } else {
        return sql;
    }
};

function parseMongoString(mongoString) {
    // Collection names should begin with an underscore or a letter character, max 128 characters
    let isValid = /^db\.[a-zA-_]\w{0,127}\.find\( *{.*} *\);$/.test(mongoString);
    
    if (!mongoString || !isValid) {
        throw new Error(INVALID_MONGO_STRING);
    }
    
    // use regex to grab the find query substring as the 3rd element in the regex match array
    let findQuery = mongoString.match(/^(db\.)([a-zA-_]\w{0,127})(\.find\()( *{.*} *)(\);)$/);
    
    //check collection name for illegal characters
    let collectionName = findQuery[2];
    let invalidCollectionName = /\\\."\$*<>:\|\?/.test(collectionName);
    if (invalidCollectionName)
        throw new Error(INVALID_MONGO_COLLECTION_NAME);
    
    let findParameters = Hjson.parse('[' + findQuery[4] + ']');
    
    return { findParameters, collectionName };
}

function getSqlForAndOr(conditions, andOr) {
    let nestedSqlClauses = conditions[andOr].reduce((accumulator, nestedClauses) => {
        Object.keys(nestedClauses).forEach((key) => {
            let nestedCondition = nestedClauses[key];
            if (key === '$and' || key === '$or') {
                accumulator.push(getWhereSql(nestedClauses));
            }
            else if (Array.isArray(nestedCondition)) {
                nestedCondition.forEach((condition) => {
                    accumulator.push(getWhereSql(condition));
                });
            }
            else {
                accumulator.push(getWhereSql(nestedCondition, key));
            }
        });
        return accumulator;
    }, []);

    if (andOr === '$or')
        return `(${nestedSqlClauses.join(' OR ')})`; 
    else // default to $and
        return `(${nestedSqlClauses.join(' AND ')})`; 
}

function getSqlForComparator(conditions, comparotor) {

    let sqlClauses = [];
    if (conditions.$gte && typeof (conditions.$gte) === 'number') {
        let sql = `${comparotor} >= ${sqlEncode(conditions.$gte)}`;
        sqlClauses.push(sql);
    }
    if (conditions.$gt && typeof (conditions.$gt) === 'number') {
        let sql = `${comparotor} > ${sqlEncode(conditions.$gt)}`;
        sqlClauses.push(sql);
    }
    if (conditions.$lte && typeof (conditions.$lte) === 'number') {
        let sql = `${comparotor} <= ${sqlEncode(conditions.$lte)}`;
        sqlClauses.push(sql);
    }
    if (conditions.$lt && typeof (conditions.$lt) === 'number') {
        let sql = `${comparotor} < ${sqlEncode(conditions.$lt)}`;
        sqlClauses.push(sql);
    }
    if (conditions.$ne) {
        let sql = `${comparotor} <> ${sqlEncode(conditions.$ne)}`;
        sqlClauses.push(sql);
    }
    if (conditions.$in) {
        let parsedInStrings = conditions.$in.map((item) => {
            return sqlEncode(item);
        });
        let sql = `${comparotor} in (${parsedInStrings.join(',')})`;
        sqlClauses.push(sql);
    }
    return sqlClauses;
}
